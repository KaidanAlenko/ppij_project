﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;

public class Container : MonoBehaviour
{
    private Settings settings;
    private List<GameObject> placeableObjectsInSlot;

    void Start()
    {
        settings = DBAdapter.SettingsDataDAOcustom.get();   //svaki put kad se ucita scena, ne mogu se settingsi promijenit usred scene
        placeableObjectsInSlot = new List<GameObject>();    //tu je prije bio bug dok je sve bilo static, ostali su od prosle scene objekti unutra
    }

    public void placeableObjectAddedToSlot(GameObject placeableObject)
    {
        if (placeableObject != null)
        {
            if (!settings.AllowDuplicates)
            {
                placeableObjectsInSlot.Add(placeableObject);

                //ako nisu dopušteni duplikati, ukloni odabranu kuglicu iz 
                //onih koji se mogu izabrati
                placeableObject.SetActive(false);
            }
        }
    }

    public void placedObjectRemovedFromSlots(Sprite objectSprite)
    {
        if (!settings.AllowDuplicates)
        {
            findBySprite(objectSprite).SetActive(true);
        }
    }

    public void moveFinished()
    {
        if (!settings.AllowDuplicates)
        {
            foreach (GameObject gameObject in placeableObjectsInSlot)
            {

                gameObject.SetActive(true);
            }

            placeableObjectsInSlot.Clear();
        }
    }

    private GameObject findBySprite(Sprite objectSprite)
    {
        foreach (GameObject gameObject in placeableObjectsInSlot)
        {
            if (gameObject.GetComponent<SpriteRenderer>().sprite == objectSprite)
            {
                return gameObject;
            }
        }

        return new GameObject();
    }
}
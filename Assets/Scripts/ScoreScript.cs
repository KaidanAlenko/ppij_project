﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ScoreScript : MonoBehaviour
{
    public Text scoreLabel;

    private Settings settings;

    private float timeSinceLastDecrease = 1f;
    // Use this for initialization
    void Start()
    {
        settings = DBAdapter.SettingsDataDAOcustom.get();
        Update();
    }

    // Update is called once per frame
    void Update()
    {
        if (settings.HardcoreMode)
        {
            if (timeSinceLastDecrease >= 1f)
            {
                int currentScore = GameController.GetCurrentScore();
                float score = currentScore - (currentScore/100);
                GameController.SetCurrentScore(score);
                timeSinceLastDecrease = 0;
            } else
            {
                timeSinceLastDecrease += Time.deltaTime;
            }
        }
        scoreLabel.text = "Score: " + GameController.GetCurrentScore();
    }
}

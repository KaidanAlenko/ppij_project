﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;
using System;

public class SaveHighScoreOnClick : MonoBehaviour {

    public Text nickName;
    public Text highScoreText;
    public Text placeholder;
    public Text achievementUnlockedText;

    public Sprite spriteSetRepresentative1;
    public Sprite spriteSetRepresentative2;
    public Sprite spriteSetRepresentative3;

    public InputField nickNameField;
    private RangListaDAO dao;
    private bool isHighscore;

    public void SaveHighScoreAndPlayAgain()
    {
        SaveHighscore();
        Settings settings = DBAdapter.SettingsDataDAOcustom.get();
        if (settings.ChosenPlaceableObjectsNum < settings.MaxPlaceableObjectsNum)
        {
            settings.ChosenPlaceableObjectsNum++;

        }
        if (settings.ChosenSlotsNum < settings.MaxSlotsNum)
        {
            settings.ChosenSlotsNum++;

        }
        DBAdapter.SettingsDataDAOcustom.update(settings);   //spremi settingse u bazu
        DefaultSettingsLoader.updateSpriteSet(DefaultSettingsLoader.gameLooksSet(settings.SpriteSet)); //obavezno nakon SettingsDataDAOcustom.update()
        SceneManager.LoadScene("PlayScene");
    }

    public void SaveHighScoreAndContinue()
    {
        SaveHighscore();
        SceneManager.LoadScene("MainMenuScene");
    }

    private void SaveHighscore()
    {
        if (isHighscore)
        {
            Rang novi = new Rang(nickName.text, GameController.GetCurrentScore().ToString());
            dao.add(novi);
        }
    }

	// Use this for initialization
	void Start () {
        dao = new RangListaDAO();
        isHighscore = false;
        List<Rang> rangLista = dao.getFirstN(10);
        if (rangLista.Count < 10)
        {
            isHighscore = true;
        }
        else
        {
            Rang rang = rangLista[rangLista.Count - 1];
            Debug.Log(rang.Score + " a novi " + GameController.GetCurrentScore());
            int score = int.Parse(rang.Score);
            if (score < GameController.GetCurrentScore())
            {
                isHighscore = true;
            }
        }
        if (!isHighscore)
        {
            Destroy(placeholder);
            Destroy(nickName);
            Destroy(highScoreText);
            nickNameField.transform.position = new Vector3(20, 0, 0);
            Destroy(nickNameField);
        }

        Settings settings = DBAdapter.SettingsDataDAOcustom.get();
        if (GameController.GetCurrentScore() > 700 
            && settings.UnlockedSpriteSetsNum < Enum.GetNames(typeof(DefaultSettingsLoader.GameLooks)).Length)
        {
            achievementUnlockedText.text = "You have unlocked:\n new placeable object set"; //todo
            settings.UnlockedSpriteSetsNum++;
            DBAdapter.SettingsDataDAOcustom.update(settings);
        }
    }
}

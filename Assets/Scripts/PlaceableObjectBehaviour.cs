﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlaceableObjectBehaviour : MonoBehaviour {

    public PositioningScript positioningScript;

	private SpriteRenderer placeableObjectSpriteRenderer;

	// Use this for initialization
	void Start () {
		placeableObjectSpriteRenderer = GetComponent<SpriteRenderer> ();
	}

    private bool click = false;
    private float lastClickTime = 0;
    private float catchTime = 0.35f;

    void OnMouseDown(){

        float clickTime = Time.time;
        if (click && (clickTime - lastClickTime) < catchTime)    //double click
        {
            click = false;
            List<Transform> currentMoveSlots = positioningScript.getCurrentMoveSlots();
            foreach(Transform currentMoveSlot in currentMoveSlots)
            {
                if (currentMoveSlot.childCount == 0)
                {
                    currentMoveSlot.GetComponent<SlotBehaviour>().instantiateChild(placeableObjectSpriteRenderer.sprite);
                    break;
                }
            }
        }
        else
        {
            click = true;
            Debug.Log("klik na placeable");
            DefaultSettingsLoader.GameAttemptClipboard.SelectedPlaceable = this.gameObject;
        }
        lastClickTime = clickTime;
    }
}

﻿using UnityEngine;
using System.Collections;

public class PlacedObjectBehaviour : MonoBehaviour {

    public PositioningScript positioningScript;
    public Container chosenPlaceablesContainer;
    
	private SpriteRenderer placedObjectSpriteRenderer;
    private Color selectColor = new Color(0.3f, 0.3f, 0.3f);    //gray

    // Use this for initialization
    void Start () {
		placedObjectSpriteRenderer = this.gameObject.GetComponent<SpriteRenderer> ();
	}

    private bool click = false;
    private float lastClickTime = 0;
    private float catchTime = 0.35f;

	void OnMouseDown(){
        if (!positioningScript.slotBelongsToCurrentMove(transform.parent)) //ne stavit parenta kao privatnu varijablu jer moze promijenit
        {
            return;
        }

        float clickTime = Time.time;
        if(click && (clickTime - lastClickTime) < catchTime)    //double click
        {
            click = false;
            DefaultSettingsLoader.GameAttemptClipboard.SelectedPlaced = null;

            chosenPlaceablesContainer.placedObjectRemovedFromSlots(this.gameObject.GetComponent<SpriteRenderer>().sprite);

            Destroy(this.gameObject);
            Debug.Log("radi destroy placed");
        }
        else
        {
            click = true;
            //make this object selected
            DefaultSettingsLoader.GameAttemptClipboard.SelectedPlaced = this.gameObject;
            placedObjectSpriteRenderer.color = selectColor;
            Debug.Log("radi select placed");
        }
        lastClickTime = clickTime;
	}
}

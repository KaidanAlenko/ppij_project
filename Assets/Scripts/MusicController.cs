﻿using UnityEngine;
using System.Collections;
using System;

public class MusicController : MonoBehaviour {
	private static MusicController instance = null;	//a la singleton
	public static MusicController Instance {
		get {
			return instance;
		}
	}

	// Use this for initialization
	void Start () {
		try{
		    Settings settings = DBAdapter.SettingsDataDAOcustom.get ();
		
		    if (instance != null/* || !settings.EnableSound*/) {
			    Debug.Log ("destroying music object clone");
			    Destroy (this.gameObject);
			    return;
		    }
		    Debug.Log ("setting music controler instance");
		    instance = this;
		    DontDestroyOnLoad (instance.gameObject);
            if (!settings.EnableSound)
            {
                instance.gameObject.GetComponent<AudioSource>().Pause();
            }
		}catch(Exception e){
			DefaultSettingsLoader.debugLogStatic.text = e.ToString ();
		}
	}

	public static void stopMusic(){
		if (instance != null) {
            instance.gameObject.GetComponent<AudioSource>().Pause();
            /*Destroy (instance.gameObject);	//unisti objekt s muzikom da ne svira vise
			instance = null;*/
        }
	}

    public static void resumeMusic()
    {
        if (instance != null)
        {
            instance.gameObject.GetComponent<AudioSource>().UnPause();
        }
    }
}

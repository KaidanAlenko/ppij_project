﻿using System;
using System.Collections.Generic;

public class CodeGenerator
{
    private static Random random = new Random();

	/// <summary>
	/// Generates the secret code.
	/// </summary>
	/// <returns>The secret code.</returns>
	/// <param name="numberOfSlots">Number of slots.</param>
	/// <param name="numberOfBalls">Number of balls.</param>
	/// <param name="allowDuplicates">If set to <c>true</c> allow duplicates.</param>
    public static List<int> generateSecretCode(int numberOfSlots, int numberOfBalls, bool allowDuplicates)
    {
        List<int> codedBallIndexes = new List<int>();
        List<int> unusedBallIndexes = generateIndexes(numberOfBalls);
        
        for (int i = 0; i < numberOfSlots; i++)
        {
            int rndNumber = random.Next(0, unusedBallIndexes.Count - 1);
            codedBallIndexes.Add(unusedBallIndexes[rndNumber]);

            if (!allowDuplicates)
            {
                unusedBallIndexes[rndNumber] = unusedBallIndexes[unusedBallIndexes.Count - 1]; //O(1)
                unusedBallIndexes.RemoveAt(unusedBallIndexes.Count - 1); //O(1)
			}
        }
		return codedBallIndexes;
    }

	/// <summary>
	/// Generates the indexes.
	/// </summary>
	/// <returns>The indexes.</returns>
	/// <param name="length">Length.</param>
    private static List<int> generateIndexes(int length)
    {
        List<int> indexes = new List<int>();
        for (int i = 0; i < length; i++)
        {
            indexes.Add(i);
        }
        return indexes;
    }
}

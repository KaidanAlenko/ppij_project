﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Kao clipboard za objekte i spriteove koji se trebaju pamtiti u trenutnoj igri.
/// Ono sto ne moze u obicnu bazu podataka (npr niz spriteova).
/// </summary>
public class GameAttemptClipboard {

    private Color deselectColor = new Color(1f, 1f, 1f);

    /// <summary>
    /// Gets or sets the sprites of the main placeable object
    /// </summary>
    /// <value>The sprites.</value>
    public Sprite[] SlicedChosenSprites { get; set; }


	private GameObject selected;

	public GameObject SelectedPlaced {
		get{ return selected; }
		set{ 
			if (value != null) {
                clearClipboard();
			}
            selected = value;
        }
	}
	private Sprite desiredSprite;
	public Sprite DesiredSprite{
		get{ return desiredSprite; }
		set{ 
			if (value != null) {
                clearClipboard();
            }
            desiredSprite = value;
        }
	}

    private GameObject selectedPlaceable;
    public GameObject SelectedPlaceable {
        get { return selectedPlaceable; }
        set
        {
            if (value != null)
            {
                clearClipboard();
            }
            selectedPlaceable = value;
        }
    }

    public void clearClipboard()
    {
        if (selected != null)
        {
            selected.GetComponent<SpriteRenderer>().color = deselectColor;
            selected = null;
        }
        desiredSprite = null;
        SelectedPlaceable = null;
    }
}

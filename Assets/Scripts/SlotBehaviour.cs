﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using System;

public class SlotBehaviour : MonoBehaviour{

    public PositioningScript positioningScript;
    public Container chosenPlaceablesContainer;
    
	public GameObject placedObject; //vrsta objekata za stavljanje u slotove, assignat u unityju

	void OnMouseDown(){

        Debug.Log("klik na slot");

        /*ako iz nekog nepoznatog razloga/glitcha uspijemo kliknut 
            na collider od slota dok na njemu ima djece,
            ili ako iz nekog nepoznatog razloga uspijemo kliknut na slot koji nije current move, odustani*/
        if (this.gameObject.transform.childCount > 0 || !positioningScript.slotBelongsToCurrentMove(this.gameObject.transform))
        {
            return;
        }

        //ovo je trenutno odabrani placedobject kojeg premjestamo
        GameObject selectedPlaced = DefaultSettingsLoader.GameAttemptClipboard.SelectedPlaced;

        //ovo je trenutno odabani placeableObject na ciju sliku i priliku zelimo stvoriti placedObject
        GameObject selectedPlaceable = DefaultSettingsLoader.GameAttemptClipboard.SelectedPlaceable;

        

        if (selectedPlaced != null) {
            adoptChild(selectedPlaced);
        }
        else if (selectedPlaceable != null) {
            instantiateChild(selectedPlaceable.GetComponent<SpriteRenderer>().sprite);
		}

        
    }

    /**
        gameObject je kuglica 
    */
    public static IEnumerator flashSlot(GameObject gameObject)
    {
        Sprite sprite = gameObject.GetComponent<SpriteRenderer>().sprite;
        Sprite whiteSlot = Resources.Load("SlotTextureFlash", typeof(Sprite)) as Sprite;
        gameObject.GetComponent<SpriteRenderer>().sprite=whiteSlot;

        Debug.Log("Zapocinje cekanje!");
        yield return new WaitForSeconds(0.1f);
        Debug.Log("Zavrsava cekanje!");
        gameObject.GetComponent<SpriteRenderer>().sprite = sprite;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="selectedPlaced">zeljeniPlacedObject</param>
    private void adoptChild(GameObject selectedPlaced)
    {
        selectedPlaced.transform.parent = this.gameObject.transform;        //postavi mu ovaj slot za roditelja
        selectedPlaced.transform.localPosition = Vector3.zero;   //pozicija relativna na roditelja(slot), ocito nula
        Debug.Log("radi premjestaj");
        DefaultSettingsLoader.GameAttemptClipboard.clearClipboard();

        StartCoroutine(flashSlot(selectedPlaced));
    }
    public GameObject instantiateChild(Sprite desiredSprite)
    {
        GameObject copy = Instantiate(placedObject) as GameObject;
        copy.transform.SetParent(this.gameObject.transform, false);
        copy.transform.localScale = Vector3.one;
        copy.transform.localPosition = Vector3.zero;
        copy.GetComponent<SpriteRenderer>().sprite = desiredSprite;
        copy.SetActive(true); //zato jer je originalni placed disablean
        Debug.Log("radi postavljanje");

        chosenPlaceablesContainer.placeableObjectAddedToSlot(DefaultSettingsLoader.GameAttemptClipboard.SelectedPlaceable);

        DefaultSettingsLoader.GameAttemptClipboard.clearClipboard();   //ovo mora bit u ovoj metodi jer se ova metoda moze pozvat i izvana, bez da se izvrsi OnClick();

        StartCoroutine(flashSlot(copy));

        return copy;
    }
}

﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class EndSceneMessageSetter : MonoBehaviour {

    public Text winMessage;

	// Use this for initialization
	void Start ()
    {
        winMessage.text = "You won the game in " + GameController.GetCurrentMove()
            + " moves!" + System.Environment.NewLine + "Score: " + GameController.GetCurrentScore();
	}
	
}

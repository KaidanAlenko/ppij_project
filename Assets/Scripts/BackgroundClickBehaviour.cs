﻿using UnityEngine;
using System.Collections;

public class BackgroundClickBehaviour : MonoBehaviour {

    private Color deselectColor = Color.white;

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnMouseDown(){
        
		GameObject selectedPlacedObject = DefaultSettingsLoader.GameAttemptClipboard.SelectedPlaced;
		if (selectedPlacedObject != null) {
			selectedPlacedObject.GetComponent<SpriteRenderer> ().color = deselectColor;
		}
		DefaultSettingsLoader.GameAttemptClipboard.SelectedPlaced = null;

		DefaultSettingsLoader.GameAttemptClipboard.SelectedPlaceable = null;

		Debug.Log ("radi deselect");
	}
}

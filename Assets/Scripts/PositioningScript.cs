﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;
using System;

//TODO napravit da se centriraju, da nisu skroz lijevo

/// <summary>
/// Postavlja na ekran odabrani broj placeablea i slotova i pritom pazi na dimenzije ekrana
/// </summary>
public class PositioningScript : MonoBehaviour {

	public GameObject slot;
	public GameObject placed;
	public GameObject placeable;
    public GameObject feedbackBackground;
    public GameObject feedbackObject;
    public GameObject scrollView;
    public Canvas canvas;
    public SpriteRenderer bottomBackgroundSr;
    public SpriteRenderer topBackgroundSr;

    public Button finishMoveButton;
    public Button backButton;

    private List<Transform> currentMoveSlots;   //samo oni slotovi koji su zadnji dodani, oni prethodni su popunjeni i ne smiju se moc editat

    private List<GameObject> lastFeedbackObjects = new List<GameObject>();
    
    public List<Transform> getCurrentMoveSlots()
    {
        return currentMoveSlots;
    }

    public bool slotBelongsToCurrentMove(Transform slot)
    {
        return currentMoveSlots.Contains(slot);  //O(n)
    }
    private float canvasScale = 1;
    private float northOffset = 0;
    private float southOffset = 0;
    private float southOffsetInsideScrollView = 0;
    private float spaceBetweenSlotBars = 5f;

    private float contentHeigth;
    private float contentWidth;

    private Vector2 feedbackDimensions;

    private Bar slotBarInfo, placeablesBarInfo;
    private Settings settings;

	// Use this for initialization
	void Start () {
        Sprite[] sprites = DefaultSettingsLoader.GameAttemptClipboard.SlicedChosenSprites; //pristupamo configu

        if (sprites == null)
        {
            Debug.Log("jeste li mozda zaboravili prvo izvrsiti skriptu DefaultSettingsLoader?");
            return;
        }

        Debug.Log("Setting code...");
        GameController.SetCode(); // kod za ovu igru    //mora bit pozvano prije resizeContent() jer ga koristi

        //da netko slucajno ne bi kliknuo na njih
        slot.SetActive(false);
        placed.SetActive(false); 
        feedbackBackground.SetActive(false);
        feedbackObject.SetActive(false);

        settings = DBAdapter.SettingsDataDAOcustom.get();
        currentMoveSlots = new List<Transform>();
        
        canvasScale = canvas.GetComponent<RectTransform>().lossyScale.x;
        
        SpriteRenderer feedbackBackgrSR = feedbackBackground.GetComponent<SpriteRenderer>();
        feedbackDimensions = new Vector2(feedbackBackgrSR.bounds.size.x / canvasScale, feedbackBackgrSR.bounds.size.y / canvasScale);
        float legalWorldWidthForSlotBar = getWorldDimensions().x - feedbackBackgrSR.bounds.size.x;
        slotBarInfo = new Bar(slot, settings.ChosenSlotsNum, legalWorldWidthForSlotBar, canvasScale);
        placeablesBarInfo = new Bar(placeable, settings.ChosenPlaceableObjectsNum, getWorldDimensions().x, 1);

        addButtonsHeigthToSouthVerticalOffset();
        addButtonsHeigthToNorthVerticalOffset();

        positionPlaceables();

        positionScrollView();
        positionScrollableContent();
        positionBackgrounds();
        
        positionNewSlots(); // position first slots
	}

    private void positionScrollableContent()
    {
        RectTransform scrollViewRectT = scrollView.GetComponent<RectTransform>();
        RectTransform contentRectT = slot.transform.parent.GetComponent<RectTransform>();
        contentHeigth = contentRectT.rect.height;
        contentWidth = contentRectT.rect.width;

        float newHeigth = GameController.GetMaxNumberOfMoves() * (slotBarInfo.barHeight + spaceBetweenSlotBars);
        newHeigth += slotBarInfo.ObjectDimensions.y;    //ostavimo malo prostora gore da nam zadnji korak ne bude prekriven onim tekstom
        newHeigth = Math.Max(newHeigth, scrollViewRectT.rect.height);
        contentRectT.sizeDelta = new Vector2(1, newHeigth);
        contentHeigth = newHeigth;
        scrollView.GetComponent<ScrollRect>().normalizedPosition = Vector2.zero;
    }

    private void positionBackgrounds()
    {
        float topWidth = topBackgroundSr.sprite.bounds.size.x;
        float topHeight = topBackgroundSr.sprite.bounds.size.y;


        float bottomWidth = bottomBackgroundSr.sprite.bounds.size.x;
        float bottomHeight = bottomBackgroundSr.sprite.bounds.size.y;

        Vector2 worldDimensions = getWorldDimensions();
        Vector2 bottomBackgroundTopLeftCorner = -worldDimensions / 2 + new Vector2(0, southOffset);
        Vector2 topBackgroundBottomLeftCorner = worldDimensions / 2 - new Vector2(0, northOffset);

        float centerVerticalPositionTopBackground = (topBackgroundBottomLeftCorner.y + worldDimensions.y / 2) / 2;
        float centerVerticalPositionBottomBackground = (bottomBackgroundTopLeftCorner.y - worldDimensions.y / 2) / 2;
        
        topBackgroundSr.transform.localScale = new Vector2(worldDimensions.x / topWidth, northOffset / topHeight);
        topBackgroundSr.transform.localPosition = new Vector2(0, centerVerticalPositionTopBackground);

        bottomBackgroundSr.transform.localScale = new Vector2(worldDimensions.x / bottomWidth, southOffset / bottomHeight);
        bottomBackgroundSr.transform.localPosition = new Vector2(0, centerVerticalPositionBottomBackground);
    }

    private void addButtonsHeigthToNorthVerticalOffset()
    {
        northOffset += getButtonHeigth(backButton);
    }

    private void addButtonsHeigthToSouthVerticalOffset()
    {
        southOffset += getButtonHeigth(finishMoveButton);
    }

    private float getButtonHeigth(Button button)
    {
        Vector3[] buttonCorners = new Vector3[4];
        button.GetComponent<RectTransform>().GetWorldCorners(buttonCorners);
        return Math.Abs(buttonCorners[2].y - buttonCorners[0].y);
    }

    private void positionScrollView()
    {
        Vector2 worldDimensions = getWorldDimensions();
        Vector2 bottomLeftScrollViewCorner = - worldDimensions / 2 + new Vector2(0, southOffset);
        Vector2 upperLeftScrollViewCorner = worldDimensions / 2 - new Vector2(0, northOffset);
        float centerVerticalPosition = (upperLeftScrollViewCorner.y + bottomLeftScrollViewCorner.y) / 2;
        float heigth = Math.Abs(upperLeftScrollViewCorner.y - bottomLeftScrollViewCorner.y);
        float width = worldDimensions.x;
        scrollView.transform.position = new Vector3(0, centerVerticalPosition);
        RectTransform rt = scrollView.transform.GetComponent<RectTransform>();
        rt.sizeDelta = new Vector2(width / canvasScale, heigth / canvasScale); 
    }

    /// <summary>
    /// Postavlja placeable objekte u svoje ispravne pozicije pri dnu ekrana.
    /// Uzeta u obzir i mogucnost da nece svi stati u jedan red.
    /// </summary>
    /// <returns>visinu trake placeablea</returns>
    private void positionPlaceables()
    {
        SpriteRenderer placeableObjectSpriteRenderer = placeable.GetComponent<SpriteRenderer>();  //used for changing picture/color
        
        Sprite[] spriteSet = DefaultSettingsLoader.GameAttemptClipboard.SlicedChosenSprites;
        Vector2 placeableDimensions = placeablesBarInfo.ObjectDimensions;
        Vector2 bottomLeftCorner = - getWorldDimensions() / 2 + placeableDimensions / 2
            + new Vector2(0, southOffset);

        int k = 0;
        int[] numOfPlaceablesInEachRow = placeablesBarInfo.numOfObjectsForEachRow;
        for (int i = 0; i < numOfPlaceablesInEachRow.Length; i++) //numOfPlaceablesInEachRow.Length == numberOfRequiredRows
        { 
            for (int j = 0; j < numOfPlaceablesInEachRow[i]; j++)
            {
                placeableObjectSpriteRenderer.sprite = spriteSet[k++];
                Instantiate(
                    placeableObjectSpriteRenderer,
                    new Vector2(bottomLeftCorner.x + j * placeableDimensions.x, bottomLeftCorner.y + i * placeableDimensions.y),
                    Quaternion.identity);
            }
        }
        Destroy(placeable);    //onog originalnog
        
        southOffset += placeablesBarInfo.barHeight;
        southOffset += 0.3f; //da bude malo razmak izmedju placeablea i scrolla
    }

    /// <summary>
    /// Postavlja slotove na ispravnu lokaciju unutar scrolla.
    /// Uzeta u obzir i mogucnost da nece svi stati u jedan red.
    /// </summary>
    public void positionNewSlots()
    {
        Vector2 slotDimensions = slotBarInfo.ObjectDimensions;
        currentMoveSlots.Clear();

        int[] numOfSlotsInEachRow = slotBarInfo.numOfObjectsForEachRow;
        for (int i = 0; i < numOfSlotsInEachRow.Length; i++)
        {
            for (int j = 0; j < numOfSlotsInEachRow[i]; j++)
            {
                GameObject clonedSlot = Instantiate(slot) as GameObject;
                clonedSlot.transform.SetParent(slot.transform.parent, false);
                Transform clonedSlotTrans = clonedSlot.GetComponent<Transform>();
                float x = - contentWidth/2 + j * slotDimensions.x + slotDimensions.x / 2;
                float y = - contentHeigth/2 + i * slotDimensions.y + slotDimensions.y / 2 + southOffsetInsideScrollView;
                clonedSlotTrans.localPosition = new Vector3(x, y, 0);

                clonedSlot.SetActive(true);
                currentMoveSlots.Add(clonedSlot.transform);

            }
        }
        
        southOffsetInsideScrollView += slotBarInfo.barHeight + spaceBetweenSlotBars;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="rightPositionAndColorNum">Number of right colors in right slots</param>
    /// <param name="wrongPositionRightColorNum">Number of right colors but in wrong slots</param>
    /// <param name="slotsNum">Number of slots</param>
    public void showNewFeedback(int rightPositionAndColorNum, int wrongPositionRightColorNum, int slotsNum)
    {
        if(settings.HardcoreMode)
        {
            foreach(GameObject feedback in lastFeedbackObjects)
            feedback.SetActive(false);
        }
        lastFeedbackObjects.Clear();


        /*
        treba jos cijelu ovu metodu malo ocistit od smeca i redundancija
        */
        GameObject newFeedback = Instantiate(feedbackBackground);
        lastFeedbackObjects.Add(newFeedback);
        newFeedback.transform.SetParent(feedbackBackground.transform.parent, false);
        Transform newFeedbackTrans = newFeedback.GetComponent<Transform>();
        float x = contentWidth / 2 - feedbackDimensions.x / 2;
        float y = - contentHeigth / 2 - feedbackDimensions.y/ 2 + southOffsetInsideScrollView - spaceBetweenSlotBars;
        newFeedbackTrans.localPosition = new Vector3(x, y, 0);

        newFeedback.SetActive(true);

        GameObject miniFeedback = Instantiate(feedbackObject);
        lastFeedbackObjects.Add(miniFeedback);
        SpriteRenderer miniFeedbackSR = miniFeedback.GetComponent<SpriteRenderer>();
        float width = miniFeedbackSR.sprite.bounds.size.x;
        float height = miniFeedbackSR.sprite.bounds.size.y;
        int maxInRowAndColumn = (int)Math.Ceiling(Math.Sqrt(slotsNum));
        Debug.Log(feedbackBackground.GetComponent<SpriteRenderer>().bounds.size.x);
        miniFeedback.transform.localScale = new Vector2(width / maxInRowAndColumn, height / maxInRowAndColumn);
        
        Bar feedbackBar = new Bar(miniFeedback, slotsNum,
            feedbackBackground.GetComponent<SpriteRenderer>().sprite.bounds.size.x, canvasScale);

        
        Vector2 feedbackMiniDim = feedbackBar.ObjectDimensions;
        int[] numOfSlotsInEachRow = feedbackBar.numOfObjectsForEachRow;
        int k = slotsNum;
        for (int i = 0; i < maxInRowAndColumn; i++)
        {
            for (int j = 0; j < maxInRowAndColumn; j++)
            {
                GameObject clonedFeedbackMini = Instantiate(miniFeedback) as GameObject;
                lastFeedbackObjects.Add(clonedFeedbackMini);
                clonedFeedbackMini.transform.SetParent(newFeedbackTrans, false);
                Transform clonedMiniFeedbackTrans = clonedFeedbackMini.GetComponent<Transform>();
                SpriteRenderer clonedMiniFeedbackSR = clonedFeedbackMini.GetComponent<SpriteRenderer>();
                x = -feedbackMiniDim.x*maxInRowAndColumn / 2 + j * feedbackMiniDim.x + feedbackMiniDim.x / 2;
                y = feedbackMiniDim.x * maxInRowAndColumn / 2 - i * feedbackMiniDim.y - feedbackMiniDim.y / 2;
                Debug.Log(feedbackMiniDim.x);
                clonedMiniFeedbackTrans.localPosition = new Vector3(x * canvasScale, y*canvasScale, 0);
                
                currentMoveSlots.Add(clonedFeedbackMini.transform);
                
                if (rightPositionAndColorNum-- > 0)
                {
                    clonedMiniFeedbackSR.color = Color.green;
                    clonedFeedbackMini.SetActive(true);
                }
                else if(wrongPositionRightColorNum-- > 0)
                {
                    clonedMiniFeedbackSR.color = Color.yellow;
                    clonedFeedbackMini.SetActive(true);
                }
                if (--k == 0)
                {
                    return;
                }
            }
        }
    }

    /// <summary>
    /// Vraca dimenzije ekrana
    /// </summary>
    /// <returns></returns>
    public static Vector2 getWorldDimensions()
    {
        float worldHeight = Camera.main.orthographicSize * 2;
        float worldWidth = worldHeight / Screen.height * Screen.width;
        return new Vector2(worldWidth, worldHeight);
    }
}

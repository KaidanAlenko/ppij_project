﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class InitialiseMovesAndCombinations : MonoBehaviour {

    public Text movesAndCombinationsText;

	void Start () {
        movesAndCombinationsText.text = "Remaining moves: " + GameController.GetCurrentNumberOfMoves()
            + System.Environment.NewLine + "Possible combinations:" + System.Environment.NewLine;
	}
	
}

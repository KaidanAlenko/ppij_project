﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;
using System.Collections.Generic;

/**

*/
public class UtilityFuntionsButtonOnClickScript : MonoBehaviour
{
    public PositioningScript positioningScript;
    public Container chosenPlaceablesContainer;
    public Button backButton;
    public Text backButtonText;
    public Text movesAndCombinationsText;

    /// <summary>
    /// Loads a scene
    /// </summary>
    /// <param name="sceneName"></param>
    public void LoadScene(string sceneName)
    {
        Debug.Log("Loading scene " + sceneName);
        SceneManager.LoadScene(sceneName);
    }

    public void FinishMove()
    {
        DefaultSettingsLoader.GameAttemptClipboard.clearClipboard();
        // metoda koja će provjeriti potez i instancirati stvari
        // za idući ako igra nije gotova

        bool validMove = true;
        List<Transform> currSlots = positioningScript.getCurrentMoveSlots();
        List<int> ids = new List<int>();
        foreach (var s in currSlots)
        {
            if (s.childCount == 0) // ako neki slot nije postavljen, potez neispravan
            {
                validMove = false;
                break;
            }
            string name = s.GetChild(0).GetComponent<SpriteRenderer>().sprite.name;
            ids.Add(DefaultSettingsLoader.getId(name));
        }
        if (validMove == false)
        {
            Debug.Log("Invalid move!");
            return;
        }

        chosenPlaceablesContainer.moveFinished();

        if (GameController.gameOver == true)
        {
            return;
        }

        Debug.Log("Valid move");
        for (int i = 0; i < ids.Count; i++)
        {
            Debug.Log(ids[i]);
        }

        GameController.SetPlayerMove(ids);
        GameController.IncrementMove();
        int[] fb = GameController.GetFeedback();
        if (fb[0] == GameController.NumberOfSlots()) // ako su pogodjeni svi slotovi
        {
            Debug.Log("Kraj igre. Pobjeda!");
            GameController.gameOver = true;
            positioningScript.getCurrentMoveSlots().Clear();
            /*backButtonText.text = "Continue";
            backButton.onClick.RemoveAllListeners();
            backButton.onClick.AddListener(() =>
                {
                    SceneManager.LoadScene("EndGameScene");
                });
                */

            SceneManager.LoadScene("EndGameScene");

            Debug.Log("Ukupan score: " + GameController.GetCurrentScore());
            return;
        }
        // ako je potez valjan i igra nije gotova, pozicioniraj sljedeće slotove
        //smanji score i vrati feedback
        Debug.Log("Tocni: " + fb[0] + " Pozicija: " + fb[1]);
        positioningScript.showNewFeedback(fb[0], fb[1], GameController.NumberOfSlots());
        GameController.LowerScoreByMove(fb[0], fb[1]);
        GameController.LowerMoves();

        if (GameController.GetCurrentNumberOfMoves() < 1 || GameController.GetCurrentScore() < 0)
        {
            Debug.Log("Kraj igre. Izgubili ste!");
            GameController.gameOver = true;
            positioningScript.getCurrentMoveSlots().Clear();
            backButtonText.text = "Back to main menu";
            movesAndCombinationsText.text = "You lose!";
            movesAndCombinationsText.color = Color.red;
            return;
        }

        Debug.Log("Current score: " + GameController.GetCurrentScore());
        Debug.Log("Remaining moves: " + GameController.GetCurrentNumberOfMoves());
        positioningScript.positionNewSlots();

        movesAndCombinationsText.text = "Remaining moves: " + GameController.GetCurrentNumberOfMoves();
    }

    public void CloseApplication()
    {
        Application.Quit();
    }

    // Use this for initialization
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {

    }
}

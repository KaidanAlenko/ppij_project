﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class Bar
{
    public Vector2 ObjectDimensions {get; private set; }
    public int numOfRequiredRows { get; private set; }
    public int[] numOfObjectsForEachRow { get; private set; }
    public float barHeight { get; private set; }

    private float legalWorldWidth;
    public Bar(GameObject gameObject, int numOfGameObjects, float legalWorldWidth, float scaleRelativeToWorld)
    {
        this.legalWorldWidth = legalWorldWidth;
        Vector2 size = gameObject.GetComponent<SpriteRenderer>().bounds.size;
        numOfRequiredRows = getNumberOfRequiredRows(numOfGameObjects, size.x);
        numOfObjectsForEachRow = getNumOfObjectsInEachRow(numOfRequiredRows, numOfGameObjects);

        ObjectDimensions = new Vector2(size.x / scaleRelativeToWorld, size.y / scaleRelativeToWorld);

        barHeight = numOfRequiredRows * ObjectDimensions.y;
    }


    /// <summary>
    /// Vraca koliko objekata stane u koji red
    /// </summary>
    /// <param name="numberOfRequiredRows"></param>
    /// <param name="objectsNum"></param>
    /// <returns></returns>
    private int[] getNumOfObjectsInEachRow(int numberOfRequiredRows, int objectsNum)
    {
        int[] numOfPlaceablesInEachRow = new int[numberOfRequiredRows];
        int remainingNums = objectsNum;
        for (int i = 0; i < numberOfRequiredRows; i++)
        {
            numOfPlaceablesInEachRow[i] = getNumOfObjectsInNextRow(remainingNums, numberOfRequiredRows - i);
            remainingNums -= numOfPlaceablesInEachRow[i];  //remainingNums se koristi u sljedecoj iteraciji petlje
        }
        return numOfPlaceablesInEachRow;
    }

    //broj loptica kroz kolikomulopticastaneujedanred = kolikoredovaimaukupno
    // int preostaloRedaka
    // brojloptica kroz preostali broj predaka = broj loptica u trenutnom redu


    /// <summary>
    /// Nadji potreban broj redaka s obzirom na broj objekata i sirinu objekta
    /// </summary>
    /// <param name="objectsNum"></param>
    /// <param name="objectWidth"></param>
    /// <returns></returns>
    private int getNumberOfRequiredRows(int objectsNum, float objectWidth)
    {
        return Convert.ToInt32(
            Math.Ceiling(objectsNum /
                Convert.ToDouble(maxObjectsInARow(objectsNum, objectWidth))));
    }

    /// <summary>
    /// Nalazi najveci moguci broj objekata u jednom retku ovisno o sirini ekrana i sirini objekta
    /// </summary>
    /// <param name="objectsNum"></param>
    /// <param name="objectWidth"></param>
    /// <returns></returns>
	private int maxObjectsInARow(int objectsNum, float objectWidth)
    {
        return Convert.ToInt32(Math.Floor(legalWorldWidth / objectWidth));
    }

    /// <summary>
    /// Koliko objekata ide u sljedeci red ovisno o broju preostalih objekata i broju preostalih redaka
    /// Ako se ispravno poziva, svaki put oduzimajuci iskoristene objekte i retke, 
    /// rezultira skoro pa uniformnom razdiobom broja objekata kroz retke.
    /// Npr za 8 kuglice i 3 retka, prvi poziv vraca 8/3=2, drugi poziv vraca 6/2=3, treci poziv vraca 3/1=3
    /// </summary>
    /// <param name="remainingObjectsNum"></param>
    /// <param name="remainingRows"></param>
    /// <returns></returns>
	private int getNumOfObjectsInNextRow(int remainingObjectsNum, int remainingRows)
    {
        return remainingObjectsNum / remainingRows;
    }
}

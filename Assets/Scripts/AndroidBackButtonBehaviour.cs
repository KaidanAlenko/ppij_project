﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.SceneManagement;

class AndroidBackButtonBehaviour: MonoBehaviour
{
    public string sceneNameToLoad;
    void FixedUpdate()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Debug.Log("Loading scene " + sceneNameToLoad);
            SceneManager.LoadScene(sceneNameToLoad);
        }
    }
}
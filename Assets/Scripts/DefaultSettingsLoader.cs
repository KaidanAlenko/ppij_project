﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;

public class DefaultSettingsLoader : MonoBehaviour {

	public enum GameLooks {spriteSet1, spriteSet2, spriteSet3, spriteSet4};

	private static GameAttemptClipboard gameAttemptClipboard;
	public static GameAttemptClipboard GameAttemptClipboard{ 
		get{
			return gameAttemptClipboard;
		}
	}
    
	public Sprite[] spriteSet1;
	
	public Sprite[] spriteSet2;
    
	public Sprite[] spriteSet3;

    public Sprite[] spriteSet4;

	public int minPlaceableObjectsNum = 3;
	public int minSlotsNum = 3;
	public int maxPlaceableObjectsNum = 12;
	public int maxSlotsNum = 12;

	public int defaultPlaceableObjectsNum = 4; //difficulty, s koliko kuglica igramo
	public int defaultSlotsNum = 4; //isto difficulty, koliko slotova moramo pogoditi
	public bool defaultAllowDuplicates = false;
    public bool defaultHardcoreMode = false;
	public bool defaultEnableSound = true;

	public Text debugLog;
	public static Text debugLogStatic;

    private int defaultSpriteSet = 1;
    private static Sprite[] spriteSet1static;
	private static Sprite[] spriteSet2static;
	private static Sprite[] spriteSet3static;
    private static Sprite[] spriteSet4static;

	// Always called before any Start() in entire Application
	void Awake () {
        if (gameAttemptClipboard != null)
        {
            return; //moze biti pozvan samo jednom, jel
        }
        gameAttemptClipboard = new GameAttemptClipboard();

        spriteSet1static = spriteSet2; //warning: zamijenjeni su tako da najljepse kuglice budu prve, nije mi se dalo u unity editoru rucno mijenjat sve
        spriteSet2static = spriteSet1;
		spriteSet3static = spriteSet3;
        spriteSet4static = spriteSet4;

		Debug.Log (Application.persistentDataPath);

		debugLogStatic = debugLog;
		try{
			DBAdapter.getConnection();
		}catch(Exception e){
			debugLog.text = e.ToString();
		}
        
        Settings settings = new Settings (minPlaceableObjectsNum, maxPlaceableObjectsNum, 
			minSlotsNum, maxSlotsNum, defaultPlaceableObjectsNum, defaultSlotsNum, 
            defaultAllowDuplicates, defaultHardcoreMode, defaultEnableSound, defaultSpriteSet);
		DBAdapter.SettingsDataDAOcustom.addIfEmpty (settings); //dodavanje samo prvi prvom pokretanju aplikacije ikad, dalje se pamte ti zapamceni settingsi
        settings = DBAdapter.SettingsDataDAOcustom.get();
        
        updateSpriteSet(gameLooksSet(settings.SpriteSet));
	}

	/// <summary>
	/// Returns a new Sprite[], constructed from source. 
	/// The returned array will contain all the elements of the source 
	/// array, from the element at index start to the element at index end.
	/// All the elements that are located at index greater than end in the 
	/// source array will not exist in the returned array.
	/// </summary>
	/// <param name="source">Source.</param>
	/// <param name="start">Start.</param>
	/// <param name="end">End.</param>
	public static Sprite[] Slice(Sprite[] source, int start, int end){
		if (end < 0) {
			end += source.Length;
		}
		int length = end - start;

		Sprite[] result = new Sprite[length];
		for(int i=0; i< length; i++){
			result [i] = source [i + start];
		}
		return result;
	}

    private static Dictionary<string, int> nameToIdSpriteMap = new Dictionary<string, int>();

    public static int getId(string spriteName)
    {
        int id;
        nameToIdSpriteMap.TryGetValue(spriteName, out id);
        return id;
    }
    
    /// <summary>
    /// Stavi odabrani sprite set u clipboard
    /// </summary>
    /// <param name="spriteSetId"></param>
	public static void updateSpriteSet(GameLooks gameLooks){
		Sprite[] chosenSlicedSet;
		
        Settings settings = DBAdapter.SettingsDataDAOcustom.get (); //treba nam da vidimo koliko je odabrano placeablea

		if (gameLooks == GameLooks.spriteSet1)
        {
			chosenSlicedSet = Slice (spriteSet1static, 0, settings.ChosenPlaceableObjectsNum);
		}
        else if (gameLooks == GameLooks.spriteSet2)
        {
			chosenSlicedSet = Slice (spriteSet2static, 0, settings.ChosenPlaceableObjectsNum);
		}
        else if(gameLooks == GameLooks.spriteSet3)
        {
            chosenSlicedSet = Slice(spriteSet3static, 0, settings.ChosenPlaceableObjectsNum);
        }
        else
        {
            chosenSlicedSet = Slice(spriteSet4static, 0, settings.ChosenPlaceableObjectsNum);
        }

        if (settings.ChosenPlaceableObjectsNum > chosenSlicedSet.Length)
        {
            Debug.Log(settings.ChosenPlaceableObjectsNum + "-" + chosenSlicedSet.Length);
            Debug.Log("odabrano je vise objekata nego koliko imamo slika-------------------------------");
            gameAttemptClipboard = null;
            return;
        }
        nameToIdSpriteMap.Clear();
        for(int i=0; i<chosenSlicedSet.Length; i++)
        {
            nameToIdSpriteMap.Add(chosenSlicedSet[i].name, i);
        }
        GameAttemptClipboard.SlicedChosenSprites = chosenSlicedSet;
	}

    public static DefaultSettingsLoader.GameLooks gameLooksSet(int i)
    {
        switch (i)
        {
            case 1:
                return DefaultSettingsLoader.GameLooks.spriteSet1;
            case 2:
                return DefaultSettingsLoader.GameLooks.spriteSet2;
            case 3:
                return DefaultSettingsLoader.GameLooks.spriteSet3;
            case 4:
                return DefaultSettingsLoader.GameLooks.spriteSet4;
            default:
                return DefaultSettingsLoader.GameLooks.spriteSet1;
        }
    }
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System;

public class StatisticsController : MonoBehaviour {

    private RangListaDAO dao;
    private List<Rang> rangLista;
    private Text highscoreText;

	// Use this for initialization
	void Start () {
	    dao = new RangListaDAO();
        rangLista = dao.getFirstN(10);
        highscoreText = GetComponent<Text>();
        int i = 1;
        if (rangLista.Count == 0)
        {
            highscoreText.text = "There are no highscores yet";
        }
        foreach (Rang r in rangLista)
        {
            highscoreText.text += String.Format("{0}    {1, -10}{2, 10}", i++ + ".", r.Nickname, r.Score)
                + Environment.NewLine;

        }
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}

﻿using System;
using UnityEngine;
using System.Data;
using Mono.Data.Sqlite;
public abstract class DatabaseTableCreator
{
	public abstract string tableName();
	public abstract string createTableStatement ();

	public DatabaseTableCreator(){
		IDbCommand dummy = null;
		IDbCommand create = null;
		try{
			try{
				dummy = DBAdapter.getConnection().CreateCommand();

				dummy.CommandText = "SELECT * FROM "+tableName();
				dummy.ExecuteNonQuery();
			}catch(SqliteException){
				create = DBAdapter.getConnection().CreateCommand();
				create.CommandText = createTableStatement();
				create.ExecuteNonQuery();
			}
		}finally{
			if (dummy != null) dummy.Dispose ();
			if (create != null) create.Dispose();
		}
	}
}


﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Mono.Data.Sqlite;
using System.Data;
using System;

public class RangListaDAO: DatabaseTableCreator, IDAO<Rang, int>{

	public override string tableName ()
	{
		return "RangLista";
	}


	public override string createTableStatement ()
	{
		return "CREATE TABLE RangLista (" +
			"`id` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE," +
			"`nickname` TEXT NOT NULL DEFAULT 'anonymous'," +
			"`score` INTEGER NOT NULL DEFAULT 0" +
			")";
	}

	/// <summary>
	/// Add the specified nickname and highscore.
	/// </summary>
	/// <param name="nickname">Nickname.</param>
	/// <param name="highscore">Highscore.</param>
	public void add(Rang rang){
		string query = "INSERT INTO RangLista(nickname,score) VALUES(\""+rang.Nickname+"\","+rang.Score+")";

		using (IDbCommand dbcmd= DBAdapter.getConnection().CreateCommand()){
			
			dbcmd.CommandText = query;
			dbcmd.ExecuteNonQuery ();
		}
		query = "SELECT last_insert_rowid()";
		using (IDbCommand dbcmd = DBAdapter.getConnection().CreateCommand()){

			dbcmd.CommandText = query;
			using (IDataReader reader = dbcmd.ExecuteReader ()) {

				if (reader.Read ()) {
					int id = reader.GetInt32 (0);
					rang.GeneratedId = id;
				}
			}
		}
	}

	/// <summary>
	/// Update the specified rang.
	/// </summary>
	/// <param name="rang">Rang.</param>
	public void update(Rang rang){
		string query = "UPDATE RangLista SET nickname="+rang.Nickname+", score="+rang.Score+" WHERE id="+rang.GeneratedId;
		using (IDbCommand dbcmd = DBAdapter.getConnection ().CreateCommand ()) {
			dbcmd.CommandText = query;
			dbcmd.ExecuteNonQuery ();
		}
	}

	/// <summary>
	/// Delete the specified generatedId, nickname and highscore.
	/// </summary>
	/// <param name="generatedId">Generated identifier.</param>
	/// <param name="nickname">Nickname.</param>
	/// <param name="highscore">Highscore.</param>
	public void delete(Rang rang){
		string query = "DELETE FROM RangLista WHERE id=" + rang.GeneratedId;

		using (IDbCommand dbcmd= DBAdapter.getConnection ().CreateCommand ()){
			
			dbcmd.CommandText = query;
			dbcmd.ExecuteNonQuery ();
		}
	}

	public void deleteAll(){
		string query = "DELETE FROM RangLista";
		using( IDbCommand dbcmd= DBAdapter.getConnection().CreateCommand()){

			dbcmd.CommandText = query;
			dbcmd.ExecuteNonQuery ();
		}
	}


	/// <summary>
	/// Get the specified generatedId.
	/// </summary>
	/// <param name="generatedId">Generated identifier.</param>
	public Rang get(int generatedId){
		string query = "SELECT * FROM RangLista WHERE id=" + generatedId;

		Rang rang = null;
		using(IDbCommand dbcmd = DBAdapter.getConnection().CreateCommand()){
			dbcmd.CommandText = query;
			using (IDataReader reader = dbcmd.ExecuteReader ()) {
				if (reader.Read ()) {
					int id = reader.GetInt32 (0);
					string nickname = reader.GetString (1);
					string score = reader.GetString (2);

					rang = new Rang (id, nickname, score);
					//Debug.Log("id=" + id + ", nickname=" + nickname + ", score=" + score);
				}
			}
		}
		return rang;
	}

	/// <summary>
	/// Gets all.
	/// </summary>
	/// <returns>The all.</returns>
	public List<Rang> getAll(){
		string query = "SELECT id, nickname, score FROM RangLista";

		List<Rang> rangList = new List<Rang> ();

		using(IDbCommand dbcmd = DBAdapter.getConnection().CreateCommand()){
			dbcmd.CommandText = query;
			using (IDataReader reader = dbcmd.ExecuteReader ()) {

				while (reader.Read ()) {
					int id = reader.GetInt32 (0);
					string nickname = reader.GetString (1);
					string score = Convert.ToString (reader.GetInt32 (2));

					rangList.Add (new Rang (id, nickname, score));
					//Debug.Log("id=" + id + ", nickname=" + nickname + ", score=" + score);
				}
			}
		}
		return rangList;
	}

    public List<Rang> getFirstN(int n)
    {
        string query = "SELECT * FROM RangLista ORDER BY score DESC LIMIT " + n;

        List<Rang> rangList = new List<Rang>();

        using (IDbCommand dbcmd = DBAdapter.getConnection().CreateCommand())
        {
            dbcmd.CommandText = query;
            using (IDataReader reader = dbcmd.ExecuteReader())
            {

                while (reader.Read())
                {
                    int id = reader.GetInt32(0);
                    string nickname = reader.GetString(1);
                    string score = Convert.ToString(reader.GetInt32(2));

                    rangList.Add(new Rang(id, nickname, score));
                }
            }
        }
        return rangList;
    }
}

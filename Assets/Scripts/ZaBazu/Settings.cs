﻿using System;


public class Settings
{
	private int minPlaceableObjectsNum, maxPlaceableObjectsNum, minSlotsNum, maxSlotsNum;
	private int chosenPlaceableObjectsNum, chosenSlotsNum, spriteSet, unlockedSpriteSetsNum;
	private bool allowDuplicates, enableSound, hardcoreMode;

	public Settings(int minPlaceableObjectsNum, int maxPlaceableObjectsNum, int minSlotsNum, int maxSlotsNum, 
		int chosenPlaceableObjectsNum, int chosenSlotsNum, bool allowDuplicates, bool hardcoreMode, bool enableSound, int spriteSet)
	{
        this.chosenPlaceableObjectsNum = chosenPlaceableObjectsNum;
        this.chosenSlotsNum = chosenSlotsNum;
        this.allowDuplicates = allowDuplicates;
        this.hardcoreMode = hardcoreMode;
        this.enableSound = enableSound;

        this.minPlaceableObjectsNum = minPlaceableObjectsNum;
		this.maxPlaceableObjectsNum = maxPlaceableObjectsNum;
		this.minSlotsNum = minSlotsNum;
		this.maxSlotsNum = maxSlotsNum;
        this.spriteSet = spriteSet;
        this.unlockedSpriteSetsNum = 1;
	}
	public int MinPlaceableObjectsNum {
		get{ return minPlaceableObjectsNum; }
	}
	public int MinSlotsNum{
		get{ return minSlotsNum; }
	}
	public int MaxPlaceableObjectsNum {
		get{ return maxPlaceableObjectsNum; }
	}
	public int MaxSlotsNum{
		get{ return maxSlotsNum; }
	}
	public int ChosenPlaceableObjectsNum {
		get{ return chosenPlaceableObjectsNum; }
		set{ chosenPlaceableObjectsNum = value; }
	}
	public int ChosenSlotsNum{
		get{ return chosenSlotsNum; }
		set{ chosenSlotsNum = value; }
	}
	public bool AllowDuplicates {
		get{ return allowDuplicates; }
		set{ allowDuplicates = value; }
	}

    public bool HardcoreMode
    {
        get { return hardcoreMode; }
        set { hardcoreMode = value; }
    }

    public bool EnableSound {
		get{ return enableSound; }
		set{ enableSound = value; }
	}

	public void toggleAllowDuplicates(){
		AllowDuplicates = !AllowDuplicates;
	}

    public void toggleHardcoreMode()
    {
        HardcoreMode = !HardcoreMode;
    }

    public int SpriteSet
    {
        get {return spriteSet;}
        set {spriteSet = value;}
    }

    public int UnlockedSpriteSetsNum
    {
        get { return unlockedSpriteSetsNum; }
        set { unlockedSpriteSetsNum = value; }
    }

	public void toggleEnableSound(){
		EnableSound = !EnableSound;
		if (!EnableSound) {
			MusicController.stopMusic ();
		} else {
            MusicController.resumeMusic();
        }
	}
}


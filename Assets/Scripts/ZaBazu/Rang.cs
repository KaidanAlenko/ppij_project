﻿using System;

public class Rang
{
	private string nickname, score;

	public int GeneratedId { get; set;} //please use setter only in add() method of IDAO implementations
	public string Nickname { 
		get{ 
			return nickname;
		}
	}
	public string Score { 
		get{
			return score;
		}
	}

	/// <summary>
	/// Initializes a new instance of the <see cref="Rang"/> class.
	/// Use this constructor when the intention is to add it to the database
	/// </summary>
	/// <param name="nickname">Nickname.</param>
	/// <param name="highscore">Highscore.</param>
	public Rang (string nickname, string highscore)
	{
		this.nickname = nickname;
		this.score = highscore;
	}

	/// <summary>
	/// Initializes a new instance of the <see cref="Rang"/> class.
	/// Use this constructor when you get the data from the database
	/// </summary>
	/// <param name="generatedId">Generated identifier.</param>
	/// <param name="nickname">Nickname.</param>
	/// <param name="highscore">Highscore.</param>
	public Rang(int generatedId, string nickname, string highscore): this(nickname, highscore){
		this.GeneratedId = generatedId;
	}
}


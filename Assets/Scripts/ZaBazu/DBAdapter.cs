﻿using UnityEngine;
using System.Collections;
using Mono.Data.Sqlite;

using System.Data;

public class DBAdapter {

	private static IDbConnection dbconn;
	private static string connString = "URI=file:" + Application.persistentDataPath + "/baza1095.db"; // Path to database

	private static RangListaDAO rangListaDAO = new RangListaDAO();
	public static RangListaDAO RangListaDAO{ get { return rangListaDAO;} }

	private static SettingsDataDAOcustom settingsDataDAOcustom = new SettingsDataDAOcustom();
	public static SettingsDataDAOcustom SettingsDataDAOcustom{
		get{ return settingsDataDAOcustom; }
	}
	/*public static Settings Settings {
		get{ return settingsDataDAOcustom.get (); }
		set{ settingsDataDAOcustom.update (value);}
	}*/

	public static IDbConnection getConnection(){
		if(dbconn==null || dbconn.State == ConnectionState.Closed || dbconn.State == ConnectionState.Broken){
			dbconn = new SqliteConnection(connString);
			dbconn.Open();
		}
		return dbconn;
	}


	public static void closeConnection(){
		if (dbconn != null) {
			dbconn.Close();
		}
	}
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public interface IDAO<T, Y>{
	void add (T t);
	void update(T t);
	void delete (T t);
	void deleteAll();
	T get(Y primaryKey);
	List<T> getAll();
}

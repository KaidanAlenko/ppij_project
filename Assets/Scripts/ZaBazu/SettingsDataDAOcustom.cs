﻿using UnityEngine;
using System;
using System.Data;
using System.Collections.Generic;
using Mono.Data.Sqlite;


public class SettingsDataDAOcustom: DatabaseTableCreator//, IDAO<Settings, int>
{
	public override string tableName ()
	{
		return "SettingsData";
	}

	public override string createTableStatement ()
	{
		return "CREATE TABLE `SettingsData` (" +
			"`minPlaceableObjectsNum` INTEGER NOT NULL DEFAULT 3," +
			"`maxPlaceableObjectsNum` INTEGER NOT NULL DEFAULT 12," +
			"`minSlotsNum` INTEGER NOT NULL DEFAULT 3," +
			"`maxSlotsNum` INTEGER NOT NULL DEFAULT 12," +
			"`chosenPlaceableObjectsNum` INTEGER NOT NULL DEFAULT 8 CHECK(chosenPlaceableObjectsNum >= minPlaceableObjectsNum AND chosenPlaceableObjectsNum <= maxPlaceableObjectsNum)," +
			"`chosenSlotsNum` INTEGER NOT NULL DEFAULT 4 CHECK(chosenSlotsNum >= minSlotsNum AND chosenSlotsNum <= maxSlotsNum)," +
			"`allowDuplicates` INTEGER NOT NULL CHECK(allowDuplicates in (0, 1))," +
            "`hardcoreMode` INTEGER NOT NULL CHECK(hardcoreMode in (0, 1)),"
            +
			"`enableSound` INTEGER NOT NULL CHECK(enableSound in (0, 1))," +
            "`spriteSet` INTEGER NOT NULL DEFAULT 1, " +
            "`unlockedSpriteSetsNum` INTEGER NOT NULL DEFAULT 1 " +
			");";
	}
	
	public void addIfEmpty(Settings t){
		string query = "SELECT * FROM SettingsData";

		using (IDbCommand dbcmd = DBAdapter.getConnection ().CreateCommand ()) {
			dbcmd.CommandText = query;
			using (IDataReader reader = dbcmd.ExecuteReader ()) {
				if (!reader.Read ()) {

					using (IDbCommand dbcmd2 = DBAdapter.getConnection ().CreateCommand ()) {
						string insert = "INSERT INTO SettingsData(" +
						               "minPlaceableObjectsNum, " +
						               "maxPlaceableObjectsNum, " +
						               "minSlotsNum, " +
						               "maxSlotsNum, " +
						               "chosenPlaceableObjectsNum, " +
						               "chosenSlotsNum, " +
						               "allowDuplicates, " +
                                       "hardcoreMode,"
                                       +
						               "enableSound, " +
                                       "spriteSet) VALUES (" +
						               t.MinPlaceableObjectsNum + "," +
						               t.MaxPlaceableObjectsNum + "," +
						               t.MinSlotsNum + "," +
						               t.MaxSlotsNum + "," +
						               t.ChosenPlaceableObjectsNum + "," +
						               t.ChosenSlotsNum + "," +
						               (t.AllowDuplicates ? 1 : 0) + "," +
                                       (t.HardcoreMode ? 1 : 0) + ","
                                       +
						               (t.EnableSound ? 1 : 0) + "," +
                                       (t.SpriteSet) + ")";

						dbcmd2.CommandText = insert;

						dbcmd2.ExecuteNonQuery ();
						Debug.Log ("dodane defaultne postavke u bazu");
					}
				}
			}
		}
	}

	public void update(Settings t){
		string update = "UPDATE SettingsData SET" +
			" minPlaceableObjectsNum=" + t.MinPlaceableObjectsNum +
			", maxPlaceableObjectsNum=" + t.MaxPlaceableObjectsNum +
			", minSlotsNum=" + t.MinSlotsNum +
			", maxSlotsNum=" + t.MaxSlotsNum +
			", chosenPlaceableObjectsNum=" + t.ChosenPlaceableObjectsNum +
			", chosenSlotsNum=" + t.ChosenSlotsNum +
			", allowDuplicates=" + (t.AllowDuplicates ? 1 : 0) +
            ", hardcoreMode=" + (t.HardcoreMode ? 1 : 0)
            +
			", enableSound=" + (t.EnableSound ? 1 : 0) +
            ", spriteSet=" + t.SpriteSet +
            ", unlockedSpriteSetsNum=" + t.UnlockedSpriteSetsNum;

        using (IDbCommand dbcmd = DBAdapter.getConnection().CreateCommand()){
			
			dbcmd.CommandText = update;
			dbcmd.ExecuteNonQuery();
		}
	}
	public Settings get(){
		string query = "SELECT * FROM SettingsData";

		Settings settings = null;

		using(IDbCommand dbcmd = DBAdapter.getConnection().CreateCommand()){

			dbcmd.CommandText = query;
			using(IDataReader r = dbcmd.ExecuteReader()){
				if (r.Read())
				{
					settings = new Settings (r.GetInt32 (0), r.GetInt32 (1), r.GetInt32 (2), 
						r.GetInt32 (3), r.GetInt32 (4), r.GetInt32 (5), 
						r.GetInt32 (6) != 0 ? true : false, r.GetInt32 (7) != 0 ? true : false, r.GetInt32(8) != 0 ? true : false,
                        r.GetInt32 (9));
                    settings.UnlockedSpriteSetsNum = r.GetInt32(10);
                }
			}
		}
		return settings;
	}
}


﻿using UnityEngine;
using System.Collections;

public class BackgroundResizer : MonoBehaviour {

	private SpriteRenderer backgroundSpriteRenderer;

	// Use this for initialization
	void Start () {
		backgroundSpriteRenderer = gameObject.GetComponent<SpriteRenderer> ();

		float width = backgroundSpriteRenderer.sprite.bounds.size.x;
		float height = backgroundSpriteRenderer.sprite.bounds.size.y;
        
        Vector2 worldDimensions = PositioningScript.getWorldDimensions();

		backgroundSpriteRenderer.transform.localScale = new Vector2 (worldDimensions.x / width, worldDimensions.y / height);
		backgroundSpriteRenderer.transform.localPosition = new Vector2 (0, 0);
	}
}

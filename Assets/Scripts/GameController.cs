﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;
using System.Text;

public class GameController {

    private static readonly float DUPLICATES_FACTOR = 1.0f;
    private static readonly float HARDCORE_FACTOR = 2.2f;
    private static readonly int SCORE_FACTOR = 100;
    private static List<int> code; //generirani kod
    private static List<int> playerMove; //potez igraca
    private static Settings settings;
    private static int score = 0;
    private static int remainingMoves;
    private static int maxMoves;
    private static int currentMove;

    public static bool gameOver;
    
    public static void SetCode()
    {
        // vađenje podataka iz settings i postavljanje koda
        settings = DBAdapter.SettingsDataDAOcustom.get();
        code = CodeGenerator.generateSecretCode(settings.ChosenSlotsNum,
            settings.ChosenPlaceableObjectsNum, settings.AllowDuplicates);
        playerMove = new List<int>();
        score = InitScore(settings.ChosenPlaceableObjectsNum, settings.ChosenSlotsNum,
            (settings.AllowDuplicates == true ? 1 : 0), settings.HardcoreMode ? 1 : 0);
        remainingMoves = InitMoves(settings.ChosenPlaceableObjectsNum, settings.ChosenSlotsNum,
            (settings.AllowDuplicates == true ? 1 : 0));
        maxMoves = remainingMoves;
        currentMove = 0;
        gameOver = false;
        Debug.Log("Max score: " + score);
    }

    public static string GetCode()
    {
        string s = "";
        foreach(int i in code)
        {
            s += i.ToString();
        }

        return s;
    }

    public static void SetPlayerMove(List<int> pMove)
    {
        playerMove = new List<int>(pMove);
    }

    public static int[] GetFeedback(/*out long numOfCombinations*/)
    {
        //Debug.Log(playerMove.Count + " " + code.Count);
        //var black = playerMove.Count(i => (playerMove[i] == code[i]));

        //var white = playerMove
        //        .Intersect(code)
        //        .Sum(c =>
        //            System.Math.Min(
        //                code.Count(x => x == c),
        //                playerMove.Count(x => x == c))) - black;

        //Debug.Log("LINQ: Tocno " + black + " Pozicija " + white);

        int[] guesses = new int[] { 0, 0 }; //guesses[0] = tocna pozicija, guesses[1] = tocna boja
        for (int i = 0; i < playerMove.Count; i++ )
        {
            //prvo provjeri tocne, pa onda provjeri pozicije
            if (playerMove[i] == code[i])
            {
                guesses[0]++;
            }
            else {
                for (int j = 0; j < code.Count; j++)
                {
                    if (playerMove[i] == code[j])
                    {
                        //preskoci one koji su tocni
                        if (playerMove[j] != code[j])
                        {
                            guesses[1]++;
                        }
                        break;
                    }
                }
            }
        }


        /*int noCorrectlyPlaced = guesses[0];
        int noIncorrectlyPlaced = guesses[1];
        int numberIncorrect = NumberOfSlots() - noCorrectlyPlaced - noIncorrectlyPlaced;
        char[] maskCharArray = new char[NumberOfSlots()];
        for(int i=0; i < maskCharArray.Length; i++)
        {
            maskCharArray[i] = '_';
        }
        string mask = new string(maskCharArray);

        char[] maskCharArray2 = new char[NumberOfSlots() - noCorrectlyPlaced];
        for (int i = 0; i < maskCharArray.Length; i++)
        {
            maskCharArray[i] = '_';
        }
        string mask2 = new string(maskCharArray);


        List<string> maskListCorPlc = new List<string>();
        List<string> maskListIncorPlc = new List<string>();

        generateMask(0, mask, maskListCorPlc, 'X', 0, noCorrectlyPlaced);
        generateMask(0, mask2, maskListIncorPlc, 'O', 0, noIncorrectlyPlaced);

        // code je kod
        List<string> combinedMaskList = generateCombinedMaskList(maskListCorPlc, maskListIncorPlc);
        List<List<int>> finalCodeList = new List<List<int>>();
        //List<int> currentAttempt = playerMove
        //izgeneriraj listu dopuštenih boja (to su sve boje koje u trenutnom pokušaju NISU iskorištene)
        List<int> allowedColors = new List<int>();
        int numberOfSlots = NumberOfSlots();
        for (int i = 0; i < numberOfSlots; i++)
        {
            if (!playerMove.Contains(i))
            {
                allowedColors.Add(i);
            }
        }
        foreach (string combinedMask in combinedMaskList)
        {
            List<int> newCode = new List<int>();
            List<int> incorrectlyPlaced = new List<int>();
            char[] combinedMaskCharArray = combinedMask.ToCharArray();
            for (int i = 0; i < code.Count(); i++)
            {
                if (combinedMaskCharArray[i] == 'X')
                {
                    newCode[i] = playerMove[i];
                }
                else if (combinedMaskCharArray[i] == 'O')
                {
                    incorrectlyPlaced.Add(playerMove[i]);
                }
            }


        }*/

        return guesses;
    }

    private static List<string> generateCombinedMaskList(List<string> maskListCorPlc, List<string> maskListIncorPlc)
    {

        List<string> combined = new List<string>();
        foreach (string s1 in maskListCorPlc)
        {
            foreach (string s2 in maskListIncorPlc)
            {
                char[] chars1 = s1.ToCharArray();
                char[] chars2 = s2.ToCharArray();
                char[] combinedChars = new char[chars1.Length];
                chars1.CopyTo(combinedChars, 0);

                int j = 0;
                for (int i = 0; i < combinedChars.Length; i++)
                {
                    if (combinedChars[i] == '_')
                    {
                        combinedChars[i] = chars2[j++];
                    }
                }
                combined.Add(new string(combinedChars));
            }
        }
        return combined;
    }

    private static void generateMask(int index, string mask, List<string> maskList, char type, int indicator, int no)
    {
        if(indicator == no)
        {
            if (!maskList.Contains(mask))
            {
                maskList.Add(mask);
            }
        }
        else
        {
            for(int i = index; i < mask.Length; i++)
            {
                if (mask[i] != type)
                {
                    //mask[i] = type;
                    char[] maskCharArr = mask.ToCharArray();
                    maskCharArr[i] = type;
                    string newMask = new string(maskCharArr);

                    generateMask(i + 1, newMask, maskList, type, indicator + 1, no);
                    //mask[i] = '_';
                }
            }
        }
    }

    public static int NumberOfSlots()
    {
        return settings.ChosenSlotsNum;
    }

    public static int NumberOfPlaceableObjects()
    {
        return settings.ChosenPlaceableObjectsNum;
    }

    public static int InitScore(int balls, int slots, int duplicates, int hardcore)
    {
        return (int)((1 + (DUPLICATES_FACTOR * duplicates)) * (balls + slots) * SCORE_FACTOR*(1+(HARDCORE_FACTOR*hardcore)));
    }

    internal static void InitScore()
    {
        settings = DBAdapter.SettingsDataDAOcustom.get();
        InitScore(settings.ChosenPlaceableObjectsNum, settings.ChosenSlotsNum,
            (settings.AllowDuplicates == true ? 1 : 0), settings.HardcoreMode ? 1 : 0);
    }

    public static void LowerScoreByMove(int hits, int hitPos)
    {
        score -= (int) (SCORE_FACTOR * (1 / (hits + (0.5 * hitPos) + 1)));
    }

    public static int GetCurrentScore()
    {
        return score;
    }

    internal static void SetCurrentScore(float newScore)
    {
        score = Mathf.CeilToInt(newScore);
    }

    public static int InitMoves(int balls, int slots, int duplicates)
    {
        return (int)(1 + (DUPLICATES_FACTOR * duplicates)) * (balls + slots);
    }

    public static void LowerMoves()
    {
        remainingMoves--;
    }

    public static int GetCurrentNumberOfMoves()
    {
        return remainingMoves;
    }

    public static int GetMaxNumberOfMoves()
    {
        return maxMoves;
    }

    public static int GetCurrentMove()
    {
        return currentMove;
    }

    public static int IncrementMove()
    {
        return currentMove++;
    }

}

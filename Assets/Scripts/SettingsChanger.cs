﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System;

public class SettingsChanger : MonoBehaviour
{
    public GameObject numberOfColorsObject;
    public GameObject numberOfSlotsObject;

    public Button colorsBackButton;
    public Button colorsForwardButton;
    public Button slotsBackButton;
    public Button slotsForwardButton;
    public Button setBackButton;
    public Button setForwardButton;

    public Toggle duplicatesToggle;
    public Toggle hardcoreToggle;
    public Toggle musicToggle;
    public SpriteRenderer sRenderer;

    public Text myTextBox;

    private Dictionary<MyTuple, long> combinationsDictionary;

    public Sprite[] sprites;

    // Use this for initialization
    void Start()
    {
        /*if (DefaultSettingsLoader.Config ==  null) {
			Debug.Log ("Config is null, please start main menu scene first");
			Destroy (this.gameObject);
			return;
		}*/
        Settings settings = DBAdapter.SettingsDataDAOcustom.get();

        hardcoreToggle.isOn = settings.HardcoreMode;
        duplicatesToggle.isOn = settings.AllowDuplicates;
        musicToggle.isOn = settings.EnableSound;

        Debug.Log("Sprite set from database: " + settings.SpriteSet);

        checkAllowedDuplicatesSettingsSynchronized(settings);
        refreshNumberFields(settings);
        RefreshSpriteSet(settings);

        combinationsDictionary = new Dictionary<MyTuple, long>();
        combinationsDictionary = computeNumberOfCombinations();

        MyTuple myTuple = new MyTuple(settings.ChosenPlaceableObjectsNum, settings.ChosenSlotsNum, duplicatesToggle.isOn);
        long myResultValue;
        combinationsDictionary.TryGetValue(myTuple, out myResultValue);
        myTextBox.text = Convert.ToString(myResultValue);

        AddOnClickListener(colorsBackButton, "cb");
        AddOnClickListener(colorsForwardButton, "cf");
        AddOnClickListener(slotsBackButton, "sb");
        AddOnClickListener(slotsForwardButton, "sf");
        AddOnClickListener(setBackButton, "setb");
        AddOnClickListener(setForwardButton, "setf");

        AddOnValueChangedListener(duplicatesToggle, "duplicates");
        AddOnValueChangedListener(hardcoreToggle, "hardcore");
        AddOnValueChangedListener(musicToggle, "music");
    }

    private Dictionary<MyTuple, long> computeNumberOfCombinations()
    {
        Dictionary<MyTuple, long> dict = new Dictionary<MyTuple, long>();
        
        long myValue = new long();

        for (int i = 3; i <= 12; i++)
        {
            for (int j = 3; j <= 12; j++)
            {
                MyTuple key = new MyTuple(i, j, true);

                myValue = Convert.ToInt64(Math.Pow(key.x, key.y));
                dict.Add(key, myValue);

                myValue = 1;
                key = new MyTuple(i, j, false);

                if (key.x >= key.y)
                {
                    int n = key.x - key.y + 1;
                    while (n <= key.x)
                    {
                        myValue = myValue * n;
                        n++;
                    }
                    dict.Add(key, myValue);
                }

            }
        }

        return dict;
    }

    void adjustTextBox()
    {
        Settings settings = DBAdapter.SettingsDataDAOcustom.get();
        MyTuple myTuple = new MyTuple(settings.ChosenPlaceableObjectsNum, settings.ChosenSlotsNum, duplicatesToggle.isOn);
        long myResultValue;
        combinationsDictionary.TryGetValue(myTuple, out myResultValue);
        myTextBox.text = Convert.ToString(myResultValue);
        return;
    }

    void clicked(string name)
    {
        Settings settings = DBAdapter.SettingsDataDAOcustom.get();

        int unlockedSpriteSetsNum 
            = Math.Min(Enum.GetNames(typeof(DefaultSettingsLoader.GameLooks)).Length, 
            settings.UnlockedSpriteSetsNum);

        switch (name)
        {
            case "cb":
                if (settings.ChosenPlaceableObjectsNum > settings.MinPlaceableObjectsNum)
                {
                    settings.ChosenPlaceableObjectsNum--;

                }
                break;
            case "cf":
                if (settings.ChosenPlaceableObjectsNum < settings.MaxPlaceableObjectsNum)
                {
                    settings.ChosenPlaceableObjectsNum++;

                }
                break;
            case "sb":
                if (settings.ChosenSlotsNum > settings.MinSlotsNum)
                {
                    settings.ChosenSlotsNum--;
                }
                break;
            case "sf":
                if (settings.ChosenSlotsNum < settings.MaxSlotsNum)
                {
                    settings.ChosenSlotsNum++;
                }
                break;
            case "setf":
                settings.SpriteSet = (settings.SpriteSet % unlockedSpriteSetsNum) + 1;
                RefreshSpriteSet(settings);
                
                Debug.Log("SS: " + settings.SpriteSet);
                break;
            case "setb":
                settings.SpriteSet = ((((settings.SpriteSet - 2 ) % unlockedSpriteSetsNum) + unlockedSpriteSetsNum) % unlockedSpriteSetsNum) + 1;
                RefreshSpriteSet(settings);
                Debug.Log("SS: " + settings.SpriteSet);
                break;
            default:
                break;
        }

        checkAllowedDuplicatesSettingsSynchronized(settings);
        refreshNumberFields(settings);

        DBAdapter.SettingsDataDAOcustom.update(settings);   //spremi settingse u bazu
        DefaultSettingsLoader.updateSpriteSet(DefaultSettingsLoader.gameLooksSet(settings.SpriteSet)); //obavezno nakon SettingsDataDAOcustom.update()
        adjustTextBox();
    }

    private void checkAllowedDuplicatesSettingsSynchronized(Settings settings)
    {
        if (!duplicatesToggle.isOn)
        {
            if (settings.ChosenPlaceableObjectsNum < settings.ChosenSlotsNum)
            {
                settings.ChosenSlotsNum = settings.ChosenPlaceableObjectsNum;
            }
        }
    }

    void toggled(string name)
    {
        Settings settings = DBAdapter.SettingsDataDAOcustom.get();
        switch (name)
        {
            case "duplicates":
                Debug.Log("duplicates toggled");
                settings.toggleAllowDuplicates();
                checkAllowedDuplicatesSettingsSynchronized(settings);
                refreshNumberFields(settings);
                break;
            case "music":
                Debug.Log("music toggled");
                settings.toggleEnableSound();
                break;
            case "hardcore":
                Debug.Log("hardcore toggled");
                settings.toggleHardcoreMode();
                break;
            default:
                break;
        }
        DBAdapter.SettingsDataDAOcustom.update(settings);
        adjustTextBox();
    }

    private void refreshNumberFields(Settings settings)
    {
        numberOfColorsObject.GetComponent<Text>().text = settings.ChosenPlaceableObjectsNum.ToString();
        numberOfSlotsObject.GetComponent<Text>().text = settings.ChosenSlotsNum.ToString();
    }

    private void RefreshSpriteSet(Settings settings)
    {
        sRenderer.sprite = sprites[settings.SpriteSet - 1];
    }

    private void AddOnClickListener(Button button, String text)
    {
        button.onClick.AddListener(delegate
        {
            clicked(text);
        });
    }

    private void AddOnValueChangedListener(Toggle toggle, String text)
    {
       toggle.onValueChanged.AddListener(delegate
        {
            toggled(text);
        });
    }

    private class MyTuple
    {
        public int x { get; set; }
        public int y { get; set; }
        public bool b { get; set; }

        public MyTuple() { }

        public MyTuple(int x, int y, bool b)
        {
            this.x = x;
            this.y = y;
            this.b = b;
        }

        public override bool Equals(object obj)
        {
            if (!(obj is MyTuple))
                return false;
            MyTuple o = obj as MyTuple;
            return x.Equals(o.x) && y.Equals(o.y) && b.Equals(o.b);
        }

        public override int GetHashCode()
        {
            int hash = 7 + 13 * x.GetHashCode();
            hash = 7 + hash * 13 * y.GetHashCode();
            return 7 + hash * 13 * b.GetHashCode();
        }
    }
}
